#TODO Errors for non-existent IDs
require 'sinatra'
require 'tide_scraper'
require 'redis'
require 'time'
require 'json'

$redis = Redis.new
class TidesAhoy < Sinatra::Base

  before do
    content_type 'application/json'
  end

  def scrape_tides(port_id)
    scraper = TideScraper::Scraper.new
    prediction = scraper.get_prediction(port_id)
# TODO: Change this to set expiry such that we keep at min 24h of predictions
# In order to do this, get last tide and subtract 24h from it
    day_in_seconds = 60 * 60 * 24
    expiry_time = (prediction.last[:time].to_i - day_in_seconds) - Time.now.to_i

    prediction.each do |tide|
      $redis.sadd(port_id,JSON.generate(tide))
    end
    $redis.expire port_id,expiry_time
    return prediction
  end

  def get_tides(port_id)
    tides = $redis.smembers(port_id)
    if tides.size == 0
      puts 'fetching...'
# TODO catch this when it fails
      begin
        scrape_tides(port_id)
        tides = $redis.smembers(port_id)
      rescue
        puts 'Error fetching tides'
        return '{"error":"error fetching tides"}'
      end
    end

# Turn an array of JSONified strings in to native ruby data
    tides_parsed = tides.map{|tide| JSON.parse tide}
# Then turn the entire thing in to a JSON string
    return JSON.generate(tides_parsed)
  end

  def get_port_info(port_id)
    scraper = TideScraper::Scraper.new
    port_info = scraper.get_port_info(port_id)
    return JSON.generate(port_info)
  end

  get '/:port_id/?' do
    get_tides(params['port_id'])
  end

  get '/:port_id/info/?' do
    get_port_info(params['port_id'])
  end

  get '/' do
    'Usage: /port_id returns tides for that port. Example: /1234'
  end
end
