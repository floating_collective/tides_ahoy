Tides Ahoy
==========

Tides Ahoy is a ruby microservice that provides tidal information for ports
around the world.

It is designed to be run with unicorn, but with a bit of modification it can
probably run under anything (within reason).

Tides Ahoy utilises the tide_scraper gem in order to pull tidal info from the
UK Hydrographic office, and will cache them in redis in order to avoid having
to make frequent requests to the UKHO.

In order to get tidal info out of Tides Ahoy, you need to provide a port ID.
At the moment, this is just something you'll need to know but eventually I'll
probably add some searching based on tidal name or latitude or something else
like that.

How to use
----------

In order to bring Tides Ahoy up, assuming correct unicorn.rb and nginx config,
simply run `bundle exec unicorn -c unicorn.rb` in the main directory.

It is also possible to bring the server up with `rackup` (which by default
starts a webrick server running on port 9292).

Assuming Tides Ahoy is running on localhost, port 9292, it can be invoked as so
, assuming the port ID you wish to look up is 1234:

```
curl localhost:9292/1234
```

Example output:

```
[
    {
        "height": "1.7 m",
        "stage": "HW",
        "time": "2016-06-30 19:43:00 +0200"
    },
    {
        "height": "0.7 m",
        "stage": "LW",
        "time": "2016-06-30 01:21:00 +0200"
    },
    {
        "height": "0.4 m",
        "stage": "LW",
        "time": "2016-06-26 09:42:00 +0200"
    },
    {
        "height": "1.8 m",
        "stage": "HW",
        "time": "2016-06-28 04:45:00 +0200"
    },
<snip>
```

There is also a `systemd` service file and startup script if you wanna do shit
that way.
